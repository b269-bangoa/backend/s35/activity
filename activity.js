const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3002;

mongoose.connect ("mongodb+srv://yanab:admin123@zuitt-bottcamp.muceymo.mongodb.net/s35-activity?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connceted to the cloud database"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const User = mongoose.model("User", userSchema);


app.post("/signup", (req,res) => {
	User.findOne({username: req.body.username}).then((result,err) => {

		if(result != null && result.username == req.body.username) {
			return res.send('Enter a valid username!')
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save().then((savedUser, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user created!");
				}
			})
		}
	})
})


//Show registered users
app.get("/users", (req,res) => {
	User.find({}).then((result,err) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result 
			})
		}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`));